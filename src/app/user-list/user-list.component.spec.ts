import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { HttpModule }    from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { User } from './../user';

import { UserListComponent } from './user-list.component';
import { UserRowComponent } from './../user-row/user-row.component';
import { UsersService } from './../users.service';

describe('UserListComponent', () => {

  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let usersService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserListComponent, UserRowComponent ],
      imports: [HttpModule],
      providers: [
        { provide: UsersService, useClass: UsersService },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    usersService = fixture.debugElement.injector.get(UsersService);
  });

  it('should be created', () => {
    //Act
    fixture.detectChanges();
    //Assert
    expect(component).toBeTruthy();
  });

  it('should be return "si"', () => {
    //Act
    fixture.detectChanges();
    let rta = component.isSingle(true);
    //Assert
    expect(rta).toEqual('si');
  });

  it('should be return "no"', () => {
    //Act
    fixture.detectChanges();
    let rta = component.isSingle(false);
    //Assert
    expect(rta).toEqual('asass');
  });

  // it('should call getAllUsers', () => {
  //   //Arrange
  //   let mockUsers = Observable.of([
  //     new User('valentina','asas@as.co','asas'),
  //     new User('nicolas','nicolas@asas.co','asas'),
  //     new User('zulema','zule@zule.co','asas'),
  //   ]);
  //   spyOn(usersService, 'getAllUsers').and.returnValue(mockUsers);
  //   //Act
  //   fixture.detectChanges();
  //   //Assert
  //   expect(component.users.length).toEqual(3);
  //   expect(usersService.getAllUsers).toHaveBeenCalled();
  // });

  // describe("test for getUser", ()=>{

  //   it('should the users[0] be an user', () => {
  //     fixture.detectChanges();
  //     //Arrange
  //     let mockUser = Observable.of(
  //       new User('karina','asas@as.co','asas'),
  //     );
  //     spyOn(usersService, 'getUser').and.returnValue(mockUser);
  //     //Act
  //     component.getUser(1223);
  //     //Assert
  //     expect(component.users[0].name).toEqual('karina');
  //   });

  //   it('should call the getUser with a param', () => {
  //     fixture.detectChanges();
  //     //Arrange
  //     let mockUser = Observable.of(
  //       new User('karina','asas@as.co','asas'),
  //     );
  //     spyOn(usersService, 'getUser').and.returnValue(mockUser);
  //     //Act
  //     component.getUser(1223);
  //     //Assert
  //     expect(usersService.getUser).toHaveBeenCalled();
  //     expect(usersService.getUser).toHaveBeenCalledWith(1223);
  //   });

  // })


});