import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { FormSkuComponent } from './form-sku.component';

describe('FormSkuComponent', () => {
  let component: FormSkuComponent;
  let fixture: ComponentFixture<FormSkuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [ FormSkuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSkuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be skuForm created', () => {
    expect(component.skuForm).toBeTruthy();
  });

  it('should be skuField created', () => {
    expect(component.skuField).toBeTruthy();
  });

  it('should be skuNameField created', () => {
    expect(component.skuNameField).toBeTruthy();
  });

  describe("Test for skuField", ()=>{

    it("should not show an error: invalidSku", ()=>{
      component.skuField.setValue("12325432134");
      expect(component.skuField.valid).toBeTruthy();
    });

    it("should show an error: invalidSku", ()=>{
      component.skuField.setValue("fdaghsh131512");
      expect(component.skuField.invalid).toBeTruthy();
      expect(component.skuField.getError('invalidSku')).toBeTruthy();
    });

    it("should show an error: required", ()=>{
      component.skuField.setValue("");
      expect(component.skuField.invalid).toBeTruthy();
      expect(component.skuField.getError('required')).toBeTruthy();
    });

    it("should show an error in template: invalidSku", async(()=>{
      //Arrange
      let input = fixture.debugElement.query(By.css('input#skuInput')).nativeElement;
      //Act
      input.value = "987654";
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      fixture.whenStable()
      .then(()=>{
        //Asert
        let msgs: any[] = fixture.nativeElement.querySelectorAll('.ui.message');
        expect(msgs.length).toEqual(1);
        expect(msgs[0].innerHTML).toContain("SKU is invalid");
      });
    }))

    it("should show an error in template: required", async(()=>{
      //Arrange
      let input = fixture.debugElement.query(By.css('input#skuInput')).nativeElement;
      //Act
      input.value = "";
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      fixture.whenStable()
      .then(()=>{
        //Asert
        let msgs: any[] = fixture.nativeElement.querySelectorAll('.ui.message');
        expect(msgs.length).toEqual(1);
        expect(msgs[0].innerHTML).toContain("SKU is required");
      });
    }))

  });
});
